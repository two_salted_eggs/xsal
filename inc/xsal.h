#ifndef __XSAL_H__
#define __XSAL_H__

#include "xsal.h"
#include "xsal_gpio.h"
#include "xsal_lcd.h"

typedef enum
{
    XSAL_OK,
    XSAL_ERROR,
}XSAL_RET_E;

typedef enum
{
    GPIO_INFO_PROTOCOL,
    SPI_INFO_PROTOCOL,
    UART_INFO_PROTOCOL,
    LCD_7735S_PROTOCOL,
    MAX_INFO_PROTOCOL,
}XSAL_ID_E;

typedef struct __xsal
{
    XSAL_ID_E xsal_id;
    void *ops;
    char status;
}xsal_t;


int xsal_init(void);
int xsal_register(xsal_t *xsal);
int xsal_get_register(XSAL_ID_E xsal_id, void **xsal_ops);
#endif /* __XSAL_H__ */