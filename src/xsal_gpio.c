#include "xsal_gpio.h"
#include "xsal.h"
#include "driver/gpio.h"

int32_t xsal_gpio_cfg(uint32_t gpio, xsal_mode_t mode)
{
    esp_err_t ret;
    gpio_pad_select_gpio(gpio);
    switch(mode)
    {
        case GPIO_INPUT:
        {
            ret = gpio_set_direction(gpio, GPIO_MODE_INPUT);
            assert(ret==ESP_OK);
            break;
        }
        case GPIO_OUTPUT:
        {
            ret = gpio_set_direction(gpio, GPIO_MODE_OUTPUT);
            assert(ret==ESP_OK);
            break;
        }
        default:
        {
            return -1;
        }
    }
    return 0;
}

int32_t xsal_gpio_out(uint32_t gpio, uint8_t val)
{
    esp_err_t ret;
    ret = gpio_set_level(gpio, val);
    assert(ret==ESP_OK);
    return 0;
}

int32_t xsal_gpio_in(uint32_t gpio, uint8_t val)
{
    esp_err_t ret;
    ret = gpio_set_level(gpio, val);
    assert(ret==ESP_OK);
    return 0;
}


gpio_protocol gpio_protocol_t = {
    .gpio_cfg_fun = xsal_gpio_cfg,
    .gpio_out_fun = xsal_gpio_out,
    .gpio_in_fun = xsal_gpio_in,
};

xsal_t xsal_gpio_t = 
{
    .xsal_id = GPIO_INFO_PROTOCOL,
    .ops = &gpio_protocol_t,
    .status = 0,
};

void init_gpio(void)
{
    xsal_register(&xsal_gpio_t);
}
